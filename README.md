# Zaber A-Series LabVIEW Driver

This is the version control repository for Zaber's official A-Series
(ASCII protocol) LabVIEW library. This library works with both A-series
and X-series Zaber devices if they are configured to use the ASCII protocol.

The official install source for this library is via the 
[National Instruments Instrument Driver Network](http://sine.ni.com/apps/utf8/niid_web_display.download_page?p_id_guid=F70175A7AAA35676E0440021287E6E02). 
Installing via the IDN gets you the most recent version that has been
certified by National Instruments.

Documentation is available on the [Zaber website](https://www.zaber.com/wiki/Software/ASCII_Labview_Driver).

